# REST API Documentation
## System Requirement

* Ruby version 3.0.1

* Rails 6.1.3

* Postgres 13.2 with Postgis

* Elasticsearch 7.12.0

### Rails Setup

The current required Ruby version is 3.0.1. We recommend you use a Ruby version
manager to handle parallel installations of different Ruby versions.
[rbenv](https://github.com/rbenv/rbenv) and [RVM](http://rvm.io) are both
supported.

1. Install the bundle:

    ```
    bundle install
    ```

2. Create and setup de database

    ```
    bundle exec rake db:setup
    ```

3. Index data to elasticsearch

    ```
    rake index:recreate
    ```

### Running in development

Once the application has been setup, run the application in development mode:

    bundle exec rails server

### Running the tests

Execute the unit tests through [Rspec](http://rspec.info):

    bundle exec rspec

## Search

Endpoints for viewing and manipulating the data that requires no authentication.

* [CMA](#markdown-header-text-search) : `POST /v1/cma`

### Query comparable property

**URL** : `/POST ${host-gateway}/v1/cma

**BODY Parameters** :
##### Mandatory

  `at=[text]` where `at` is the latitude and longtitude.

  `transaction_type=[text]` where `type` is the list of transaction's type join by comma. Available value are 'Launch Price', 'Sale Listing', 'Sold', 'Rent Listing', 'Rented', 'Indication'.

##### Optional

  `in=[integer]` where `in` is the radius in `m`. Default is 500m.

  `property_category=[text]` where `categories` is the category of property. Available value are 'Residential Property','Commercial Property', "Agricultural", "Agricultural Land", "Agricultural Property", "Apartment", "Co-Ownership Property", Condominium", "Industrial Property", "Mixed-use", "Specialized Property", "Vacant Land", "Wooden House", and "Wooden House/Dwelling".

  `property_type=[text]` where `categories` is the list of property's categories join by comma. Available value are "Cashew", "Church", "Detached House ( Villa )", "Durian", "Dwelling", "Entertainment Building", "Factory", "Farms", "Gasoline Station","Government Institution","Guest House","Hospital", "Hotel", "Improved Land", "Link House", "Longan", "Mango", "Mixed Use Building", "Non-Improved Land", "Office Building","Orchards", "Pagoda", "Parking Facilities", "Pepper", "Power Plants", "Ranches", "Resort", "Restaurant", "Retail Space", "Rubber Plantation", "School", "Semi-Detached House ( Twin Villa )", "Shop House", "Shopping Centers", "Terraced House", "Terraced House ( Flat House )", "Terraced House (Flat House)", "Timberland", "Unimproved Land", "Warehouse"

  `road_type=[text]` where `road_type` is type of road we want to filter. Available value are "trunk", "primary", "secondary", "tertiary", "residential", "track", "service", "unclassified"

  `current_use=[text]` where `current_use` is what property is being used.. Available value are "Cashew", "Church", "Detached House ( Villa )", "Durian", "Dwelling", "Entertainment Building", "Factory", "Farms", "Gasoline Station","Government Institution","Guest House","Hospital", "Hotel", "Improved Land", "Link House", "Longan", "Mango", "Mixed Use Building", "Non-Improved Land", "Office Building","Orchards", "Pagoda", "Parking Facilities", "Pepper", "Power Plants", "Ranches", "Resort", "Restaurant", "Retail Space", "Rubber Plantation", "School", "Semi-Detached House ( Twin Villa )", "Shop House", "Shopping Centers", "Terraced House", "Terraced House ( Flat House )", "Terraced House (Flat House)", "Timberland", "Unimproved Land", "Warehouse"

  `uniform=[boolean]` where `uniform` is the option to check if property is in the same landed house.

  `backdate=[integer]` where `backdate` is to filter property in the number of previous month.

  `page=[integer]` where `page` is the number of page we want to jump to. Default page is 1.

  `per_page=[integer]` where `per_page` is the result size per page. Limit from 1 to 250 and default to 20.

**SAMEPLE URL Parameters** : `${host}/v1/cma

**Method** : `POST`

**Content-Type** : `application/json`

**Auth required** : bearer

**BODY** :
  {
    "at": "11.594853, 104.885174",
    "in": 500,
    "property_category": "Residential Property",
    "transaction_type": "Sold,Indication",
    "property_type": "Link House,Terraced House (Flat House)",
    "current_use": "Link House,Terraced House (Flat House)",
    "uniform": true,
    "backdate": 3
  }


#### Success Response

**Condition** : If all parameters are valid

**Code** : `200 OK`

**Content example**

```json
"data": [
  {
    id": 65951,
    "ref_id": "68855",
    "ref_resource": "Z1 Data",
    "listing_id": null,
    "contact_id": null,
    "owner_id": null,
    "account_id": null,
    "launch_month": 0,
    "launch_year": 2018,
    "launch_price": 93000,
    "launch_price_date": "2018-05-01",
    "sale_price": null,
    "sale_price_per_sqm": null,
    "sale_list_price": 93000,
    "sale_listing_price_per_sqm": null,
    "sold_price": 93000,
    "sold_price_per_sqm": null,
    "rent_price": null,
    "rent_price_per_sqm": null,
    "sold_price_date": null,
    "sale_list_price_date": null,
    "rented_price_date": null,
    "rent_list_price_date": null,
    "indication_price_date": null,
    "indication_price": null,
    "rent_list_price": null,
    "rent_listing_price_per_sqm": null,
    "rented_price": null,
    "rented_price_per_sqm": null,
    "street_no": "D",
    "house_no": "11D",
    "address": null,
    "full_address": "#11D, St.D, Chrang Chamreh Pir, Ruessei Kaev, Phnom Penh",
    "latitude": "11.62419673",
    "longitude": "104.8955276",
    "land_width": 4.1,
    "land_length": 21.0,
    "land_area": 86.1,
    "land_area_by_title_deed": null,
    "title_deed_no": null,
    "total_size_by_title_deed": null,
    "description": null,
    "note": null,
    "record_type": null,
    "data_source_type": null,
    "zone_type": null,
    "land_shape_type": "",
    "site_position": "Intermediate lot",
    "facing_type": null,
    "tenure_type": null,
    "building_width": 4.1,
    "building_length": 15.0,
    "building_height": null,
    "stories": 1,
    "is_rent": null,
    "is_sale": null,
    "is_appraisal": null,
    "property_category": "Residential Property",
    "property_type": "Linked House",
    "current_use": "Linked House",
    "surrounding": null,
    "property_code": null,
    "code": null,
    "topography": null,
    "status": null,
    "rating": null,
    "polygon": null,
    "current_usage": null,
    "approved_by": null,
    "approved_at": null,
    "access_roads": null,
    "neighbor_location": null,
    "site_improvement": null,
    "transaction_status": null,
    "transaction_date": null,
    "data_source_phone": null,
    "full_address_kh": null,
    "borey_id": "284",
    "borey_name": "Borey Phnom Penh Sok San Project V\n",
    "current_use_std": "Linkhouse",
    "property_type_std": "Linkhouse",
    "transaction_type":["launch_price"],
    "type": "Residential Property",
    "building_area": 92.24999999999999,
    "image": null,
    "image_left_side": null,
    "image_right_side": null,
    "image_back_side": null,
    "image_opposite": null,
      "units": [],
      "property_title_deeds": []
    }
  ],
  "current_page": 1,
  "per_page": 30,
  "total_pages": 1,
  "total_entries": 1
}
```

#### Error Responses

**Condition** : If parameters is invalid

**Code** : `402 Unprocessible Entity`

**Content** : `{}`
