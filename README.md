# REST API Documentation
## System Requirement

* Ruby version 3.0.1

* Rails 6.1.3

* Postgres 13.2 with Postgis

* Elasticsearch 7.12.0

### Rails Setup

The current required Ruby version is 3.0.1. We recommend you use a Ruby version
manager to handle parallel installations of different Ruby versions.
[rbenv](https://github.com/rbenv/rbenv) and [RVM](http://rvm.io) are both
supported.

1. Install the bundle:

    ```
    bundle install
    ```

2. Create and setup de database

    ```
    bundle exec rake db:setup
    ```

3. Index data to elasticsearch

    ```
    rake index:recreate
    ```

### Running in development

Once the application has been setup, run the application in development mode:

    bundle exec rails server

### Running the tests

Execute the unit tests through [Rspec](http://rspec.info):

    bundle exec rspec

### Running via docker

    docker-compose up

## API Version

* v1 use path /v1
* v2 use path /v2

## Search

Endpoints for viewing and manipulating the data that requires no authentication.

* [Search by query](#markdown-header-text-search) : `GET /v1/places/textsearch`
* [Search nearby](#markdown-header-nearby-search) : `GET /v1/places/nearbysearch`
* [Search by geolocation](#markdown-header-search-geolocation) : `GET /v1/places/geolocation`
* [Search nearest road](#markdown-header-search-nearest-road) : `GET /v1/places/nearestroads`
* [Search administration](#markdown-header-search-administration) : `GET /v1/administrations`
* [Query POIs](#markdown-header-query-pois) : `GET /v1/point_of_interests`
* [Search Area Name](#markdown-header-search-area-name) : `POST /v1/places/area_info`


### Text Search

Search location based on query text

**URL** : `/GET ${host}/v1/places/textsearch`

**URL Parameters** :
##### Mandatory

  `q=[text]` where `q` is the name of location.

##### Optional

  `at=[text]` where `at` is the latitude and longtitude.

  `in=[integer]` where `in` is the radius in `m`.

  `categoies=[text]` where `categories` is the list of place's categories join by comma.

  `tags=[text]` where `tags` is the list of place's tags join by comma.

  `admin_code=[text]` where `admin_code` is the place's admin code.

  `pro_code=[text]` where `pro_code` is the place's province code.

  `show_distance=[boolean]` where `show_distance` is to display the distance of place from `at`.

  `order=[text]` where `order` is the column of place data to order.

  `sort=[text]` where `sort` is the method to order.

  `page=[integer]` where `page` is the number of page we want to jump to. Default page is 1.

  `per_page=[integer]` where `per_page` is the result size per page. Limit from 1 to 50 and default to 20.

**SAMEPLE URL Parameters** : `${host}/v1/places/textsearch?q=Siem Reab&page=1&per_page=30&categories=landmark&at=11.400317,104.85968&in=1.5&show_distance=true&tags=village,commune_center&admin_code=8&&pro_code=12`

**Method** : `GET`

**Auth required** : NO

#### Success Response

**Condition** : If all parameters are valid

**Code** : `200 OK`

**Content example**

```json
{
  "data":[
    {
      "id": 2945,
      "name": "Siem Reab",
      "name_local": "Siem Reab",
      "full_name": "Siem Reab Province",
      "category": "landmark",
      "admin_code": "8",
      "pro_code": "12",
      "addr_house": "NULL",
      "addr_stree": "NULL",
      "addr_com": "Siem Reab",
      "addr_dis": "Kandal Stueng",
      "addr_pro": "Kandal",
      "survey_dat": "NULL",
      "opening": "NULL",
      "email": "NULL",
      "website": "NULL",
      "hours": "NULL",
      "note": null,
      "tags":["village"],
      "zip_code": "08012201",
      "href": null,
      "lat": "11.40031732",
      "long": "104.8596897",
      "distance": 1
    }
  ],
  "current_page": 1,
  "per_page": 30,
  "total_pages": 1,
  "total_entries": 1
}
```

#### Error Responses

**Condition** : If parameters is invalid

**Code** : `402 Unprocessible Entity`

**Content** : `{}`

### Nearby Search

Search location based on query text

**URL** : `/GET ${host}/v1/places/nearbysearch`

**URL Parameters** :

##### Mandatory

  `at=[text]` where `at` is the latitude and longtitude.

##### Optional


  `in=[integer]` where `in` is the radius in `m`.

  `categoies=[text]` where `categories` is the list of place's categories join by comma.

  `tags=[text]` where `tags` is the list of place's tags join by comma.

  `admin_code=[text]` where `admin_code` is the place's admin code.

  `show_distance=[boolean]` where `show_distance` is to display the distance of place from `at`.

  `order=[text]` where `order` is the column of place data to order.

  `sort=[text]` where `sort` is the method to order.

  `page=[integer]` where `page` is the number of page we want to jump to. Default page is 1.

  `per_page=[integer]` where `per_page` is the result size per page. Limit from 1 to 50 and default to 20.

**SAMEPLE URL Parameters** : `${host}/v1/places/nearbysearch?at=11.400317,104.85968&in=1.5&page=1&per_page=30&categories=landmark&show_distance=true&tags=village,commune_center&admin_code=8`

**Method** : `GET`

**Auth required** : NO

#### Success Response

**Condition** : If all parameters are valid

**Code** : `200 OK`

**Content example**

```json
{
  "data":[
    {
      "id": 2945,
      "name": "Siem Reab",
      "name_local": "Siem Reab",
      "category": "landmark",
      "admin_code": "8",
      "addr_house": "NULL",
      "addr_stree": "NULL",
      "addr_com": "Siem Reab",
      "addr_dis": "Kandal Stueng",
      "addr_pro": "Kandal",
      "survey_dat": "NULL",
      "opening": "NULL",
      "email": "NULL",
      "website": "NULL",
      "hours": "NULL",
      "note": null,
      "tags":["village"],
      "zip_code": "08012201",
      "href": null,
      "lat": "11.40031732",
      "long": "104.8596897",
      "distance": 1
    }
  ],
  "current_page": 1,
  "per_page": 30,
  "total_pages": 1,
  "total_entries": 1
}
```

#### Error Responses

**Condition** : If parameters is invalid

**Code** : `402 Unprocessible Entity`

**Content** : `{}`

### Search Geolocation

Search geolocation based on a point

**URL** : `/GET ${host}/v1/places/geolocation`

**URL Parameters** :

##### Mandatory

  `at=[text]` where `at` is the latitude and longtitude.

##### Optional
  `categoies=[text]` where `categories` is the list of place's categories join by comma.

  `tags=[text]` where `tags` is the list of place's tags join by comma.

  `admin_code=[text]` where `admin_code` is the place's admin code.

  `order=[text]` where `order` is the column of place data to order.

  `sort=[text]` where `sort` is the method to order.

  `page=[integer]` where `page` is the number of page we want to jump to. Default page is 1.

  `per_page=[integer]` where `per_page` is the result size per page. Limit from 1 to 50 and default to 20.

**SAMEPLE URL Parameters** : `${host}/v1/places/geolocation?at=12.168697,104.4106916&page=1&per_page=30&categories=landmark&tags=village,commune_center&admin_code=8`

**Method** : `GET`

**Auth required** : NO

#### Success Response

**Condition** : If all parameters are valid

**Code** : `200 OK`

**Content example**

```json
{
  "data":{
    "province": "Kampong Chhnang",
      "district": "Tuek Phos",
      "commune": "Krang Skear",
      "zip_code": "040809",
      "geometry":{
        "type": "Feature",
        "geometry":{
          "type": "MultiPolygon",
          "coordinates":[[[[104.389598544, 12.2875522680001 ], [104.38960114, 12.28755206 ], [104.389428061, 12.287347197…]
        }
      },
      "item_type": "admin_boundary"
  }
}
```

#### Error Responses

**Condition** : If parameters is invalid

**Code** : `402 Unprocessible Entity`

**Content** : `{}`

### Search Nearest Road

Search for the nearest road

**URL** : `/GET ${host}/v1/places/nearestroads`

**URL Parameters** :

##### Mandatory

  `at=[text]` where `at` is the latitude and longtitude.

##### Optional

  `in=[integer]` where `in` is the radius in `m`.

  `max_match=[integer]` where `max_match` is the maximum matching road.

  `road_types=[text]` where `road_type` is the list of road types seperate by comma. Available road types are ['trunk','primary','secondary','tertiary','residential','track','service','unclassified']

**SAMEPLE URL Parameters** : `${host}/v1/places/nearestroads?in=10&max_match=10&at=11.58455839035605,104.8963236808777&road_types=primary,secondary`

**Method** : `GET`

**Auth required** : NO

#### Success Response

**Condition** : If all parameters are valid

**Code** : `200 OK`

**Content example**

```json
{
  "data":[
    {
      "id": 702,
      "name": "ផ្លូវ ៣៣៧",
      "oneway": "B",
      "max_speed": 0,
      "bridge": "F",
      "tunnel": "F",
      "road_type": "tertiary",
      "distance": 1,
      "geometry":{
        "type": "Feature",
        "geometry":{
          "type": "MultiLineString",
          "coordinates": [[[104.891447, 11.5833672 ], …]]
        }
      }
    }
  ]
}
```

#### Error Responses

**Condition** : If parameters is invalid

**Code** : `402 Unprocessible Entity`

**Content** : `{}`

### Search Administration

Search for the details of administration such as commune, district, and province.

**URL** : `/GET ${host}/v1/administrations`

**URL Parameters** :

##### Optional

  `admin_code=[integer]` where `in` is the code of commune, district, or province. Without admin_code, system will return list of provinces.

  `boundary=[boolean]` where `boundary` is the option that system will include boundary in the response base on value true or false.

  `sub_level=[boolean]` where `sub_level` is the option that system will return sub_administration_level based on the value true or false.

**SAMEPLE URL Parameters** : `${host}/v1/administrations?admin_code=0408&sub_level=true&boundary=true`

**Method** : `GET`

**Auth required** : NO

#### Success Response

**Condition** : If all parameters are valid

**Code** : `200 OK`

**Content example**

```json
{
  "data":{
    "admin_code": "0408",
      "name": "Tuek Phos",
      "local_name": "ទឹកផុស",
      "administration_level": "District boundary",
      "center":{
        "type": "Feature",
        "geometry":{
          "type": "Point",
          "coordinates":[104.5258652, 12.0537265]
        }
      },
      "boundary":{
        "type": "Feature",
        "geometry":{
          "type": "MultiPolygon",
          "coordinates":[[[[104.361802518, 12.333786517 ], [104.36086362, 12.331736135 ], [104.360262067, 12.330077642…]
        }
      },
      "sub_administration_level":[
      {"admin_code": "040801", "name": "Akphivoadth", "local_name": "អភិវឌ្ឍន៍", "administration_level": "Commune boundary",…},
      {"admin_code": "040803", "name": "Chaong Maong", "local_name": "ចោងម៉ោង", "administration_level": "Commune boundary",…},
      {"admin_code": "040802", "name": "Chieb", "local_name": "ជៀប", "administration_level": "Commune boundary",…},
      {"admin_code": "040804", "name": "Kbal Tuek", "local_name": "ក្បាលទឹក", "administration_level": "Commune boundary",…},
      {"admin_code": "040809", "name": "Kdol Saen Chey", "local_name": "ក្តុលសែនជ័យ", "administration_level": "Commune boundary",…},
      {"admin_code": "040805", "name": "Khlong Popok", "local_name": "ខ្លុងពពក", "administration_level": "Commune boundary",…},
      {"admin_code": "040806", "name": "Krang Skear", "local_name": "ក្រាំងស្គារ", "administration_level": "Commune boundary",…},
      {"admin_code": "040807", "name": "Tang Krasang", "local_name": "តាំងក្រសាំង", "administration_level": "Commune boundary",…},
      {"admin_code": "040808", "name": "Tuol Khpos", "local_name": "ទួលខ្ពស់", "administration_level": "Commune boundary",…}
      ]
  }
}
```

#### Error Responses

**Condition** : If parameters is invalid

**Code** : `402 Unprocessible Entity`

**Content** : `{}`

### Query POIs

Query POIs by bounding box

**URL** : `/GET ${host}/v1/point-of-interests`

**URL Parameters** :

##### Optional

  `bbox=[text]` where `bbox` is the bounding box data that we use to query POIs within that. It is a combination of two points.
  `zoom=[integer]` where `zoom` is the zoom level.

**SAMEPLE URL Parameters** : `${host}/v1/point-of-interests?bbox=104.91478111,11.59380712,104.91177711,11.58985511&zoom=10`

**Method** : `GET`

**Auth required** : NO

#### Success Response

**Condition** : If all parameters are valid

**Code** : `200 OK`

**Content example**

```json
{
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "id": 21073,
            "properties": {
                "id": 21073,
                "name": "Da Moon",
                "name_kh": "NULL",
                "full_name": null,
                "admin_code": null,
                "category": null,
                "tags": [
                    "hair_care",
                    "health",
                    "point_of_interest",
                    "establishment"
                ]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    104.9121292,
                    11.5935486
                ]
            }
        },
        {
            "type": "Feature",
            "id": 16830,
            "properties": {
                "id": 16830,
                "name": "ឃីម គន្ធា",
                "name_kh": "NULL",
                "full_name": null,
                "admin_code": null,
                "category": null,
                "tags": [
                    "finance",
                    "point_of_interest",
                    "establishment"
                ]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    104.9145211,
                    11.5923542
                ]
            }
        },
        {
            "type": "Feature",
            "id": 3830,
            "properties": {
                "id": 3830,
                "name": "ហេង ហេង សម្បត្តិ",
                "name_kh": "NULL",
                "full_name": null,
                "admin_code": null,
                "category": null,
                "tags": [
                    "electronics_store",
                    "point_of_interest",
                    "store",
                    "establishment"
                ]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    104.911919,
                    11.5937741
                ]
            }
        }
    ]
}

```

#### Error Responses

**Condition** : If parameters is invalid

**Code** : `402 Unprocessible Entity`

**Content** : `{}`

### Search Area Name

Search for the area name based on circle or polygon

**URL** : `/POST ${host}/v1/places/area_info`

**URL Parameters** :

##### Optional

  `points=[text]` where `points` is the array of point. Inside point, there are two params `at` and `in`.

  `at=[text]` where `at` is the latitude and longtitude.

  `in=[integer]` where `in` is the radius in `m`.

  `polygons=[long, lat]` where `polygons` is the array of polygons.

**SAMEPLE URL Parameters** : `${host}/v1/places/area_info`

**Method** : `POST`

**Auth required** : NO

**Body** :
```json
{
  "points": [
    {
      "at": "12.722895685001374, 104.88170449755881",
      "in": 1000
    }
  ],
  "polygons":[
      [
          [104.8829496, 12.7203036],
          [104.8885967, 12.7203036],
          [104.8885967, 12.7231297],
          [104.8829496, 12.7231297],
          [104.8829496, 12.7203036]
      ],
      [
          [104.87368583679198,12.71356751903732],
          [104.87578868865967,12.71356751903732],
          [104.87578868865967,12.716958412799846],
          [104.87368583679198,12.716958412799846],
          [104.87368583679198,12.71356751903732]
      ]
  ]
}
```

#### Success Response

**Condition** : If all parameters are valid

**Code** : `200 OK`

**Content example**

```json
{
  "data": {
    "name_en": "Krong Stueng Saen",
    "name_kh": "ស្ទឹងសែន",
    "district_name_en": "Krong Stueng Saen",
    "district_name_kh": "ស្ទឹងសែន",
    "province_name_en": "Kampong Thom",
    "province_name_kh": "កំពង់ធំ",
    "district_code": "0603",
    "country_name_en": "Cambodia",
    "country_name_kh": "កម្ពុជា",
    "country_iso_code": "KH",
    "center": {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          104.8875231,
          12.7010225
        ]
      }
    },
    "district_boundary": {
      "type": "Feature",
      "geometry": {
        "type": "MultiPolygon",
        "coordinates": [
          [
            [
              [
                104.900764803,
                12.74119389
              ],
              [
                104.906015306,
                12.740119136
              ],
              [
                104.906274214,
                12.741945979
              ],
              ...
            ]
          ]
        ]
      }
    }
  }
}
```

#### Error Responses

**Condition** : If parameters is invalid

**Code** : `402 Unprocessible Entity`

**Content** : `{}`
